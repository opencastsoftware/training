Feature: Check weather for a specified location                                          //Simple scenario

  In order to check local weather
  As a user
  I want the system to return weather for a specified location

  Scenario: User entered a location to get local weather

    Given User navigates to BBC News website
    And User clicks on Weather menu
    And User searches for Newcastle location in the auto-complete box
    When User clicks on Search icon
    Then Weather for Newcastle upon Type location is shown


