Feature: Navigate to a chosen news category from the home page                                        //Scenario outline

  In order to get news from a specific category
  As a user
  I want the system to redirect me to a page with the news category of my chose

  Scenario Outline: User successfully re-directed to their chosen news category page

    Given User navigates to BBC News website
    When user navigates to a news <categoryName> from the home page
    Then user should be re-directed to corresponding <categoryName> news page 
    

    Examples:
        |     categoryName      |
        |        uk             |
        |        world          |
        |        business       |
        |        politics       |


