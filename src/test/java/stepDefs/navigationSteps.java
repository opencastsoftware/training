package stepDefs;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import java.lang.System;
import static utils.driverFactory.driver;
import static utils.driverFactory.navigationPage;

public class navigationSteps {

	@When("user navigates to a news (.+) from the home page")
    public void user_navigates_to_a_news_from_the_home_page(String categoryName) {
		
        switch (categoryName) {
            case "uk":
                navigationPage.uk.click();
                break;
            case "world":
                navigationPage.world.click();
                break;
            case "business":
                navigationPage.business.click();
                break;
            case "politics":
                navigationPage.politics.click();
                break;

            default:
                System.out.println("unknown category");

        }
    }


    @Then("user should be re-directed to corresponding (.+) news page")
    public void user_should_be_re_directed_to_corresponding_news_page(String categoryName) {

        String actualURL = driver.getCurrentUrl();
        String expectedURL = categoryName;
        Assert.assertTrue(actualURL.contains(expectedURL));
        System.out.print(actualURL);
    }


}
