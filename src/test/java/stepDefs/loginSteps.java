package stepDefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static utils.driverFactory.*;

public class loginSteps {

    @Given("User navigates to BBC News website")
    public void user_navigates_to_BBC_News_website() {
        driver.get("http://www.bbc.co.uk/news");
    }

    @And("User clicks on the Sign in option")
    public void user_clicks_on_the_Sign_in_option() throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(loginPage.singIn))).click();
     

    }

    @Given("User enters a valid email address")
    public void user_enters_a_valid_email_address() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(loginPage.username))).sendKeys("bellnatnat@gmail.com");;

    }

    @And("User enters a valid password")
    public void user_enters_a_valid_password() {
    	driver.findElement(By.id(loginPage.password)).sendKeys("mypassword1");


    }

    @When("User clicks on the Sign in button")
    public void user_clicks_on_the_Sign_in_button() throws Throwable {
        driver.findElement(By.id(loginPage.submit)).click();

    }

    @Then("User should be signed in successfully")
    public void user_should_be_signed_in_successfully()throws Throwable {
        WebElement category = driver.findElement(By.id(loginPage.singIn));
        String user = category.getText();
        Assert.assertEquals("Automation", user);

    }

    @Given("User enters an invalid password")
    public void user_enters_an_invalid_password() {
    	driver.findElement(By.id(loginPage.password)).sendKeys("mypassword2");

    }

    @Then("Password isn't valid message is displayed to the user")
    public void password_isn_t_valid_message_is_displayed_to_the_user() throws Throwable {
        WebElement category = driver.findElement(By.xpath(loginPage.invalidInput));
        String invalidPassword = category.getText();
        Assert.assertEquals(loginPage.invalidPasswordMessage, invalidPassword);

    }


    @Given("User enters an invalid email address")
    public void user_enters_an_invalid_email_address() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(loginPage.username))).sendKeys("bellnatnata@gmail.com");;

    }

    @Then("Account doesn't exist message is displayed to the user")
    public void account_doesn_t_exist_message_is_displayed_to_the_user() throws Throwable{
        WebElement category = driver.findElement(By.xpath(loginPage.invalidInput));
        String invalidEmail = category.getText();
        Assert.assertEquals(loginPage.noAccountMessage, invalidEmail);
        Thread.sleep(1000);
    }




}
