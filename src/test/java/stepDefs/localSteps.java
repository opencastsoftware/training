package stepDefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import static utils.driverFactory.*;

public class localSteps {

	@And("User clicks on Weather menu")
	public void user_clicks_on_Weather_menu() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(localPage.weather))).click();
	}

	@And("^User searches for Newcastle location in the auto-complete box$")
	public void user_searches_for_Newcastle_location_in_the_auto_complete_box() throws InterruptedException {
		
		//User inputs "Newcastle" into the auto-complete box and chooses "Newcastle upon Tyne" from the dropdown list
		WebDriverWait wait = new WebDriverWait(driver, 5);
		driver.findElement(By.id(localPage.locatorInput)).sendKeys("Newcastle");;
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(localPage.locationsList)));
		List<WebElement> optionsToSelect = driver.findElements(By.className(localPage.locationsListItem));

		for (WebElement option : optionsToSelect) {
			String optionText = option.getText();
		
			if (optionText.contains("Newcastle upon Tyne")) {
				option.click();
				break;
			}
		}
	}

	
	@When("User clicks on Search icon")
	public void user_clicks_on_Search_icon() {
		driver.findElement(By.xpath(localPage.searchLocation)).click();
		
	}

	@Then("^Weather for Newcastle upon Type location is shown$")
	public void weather_for_Newcastle_upon_Type_location_is_shown() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		WebElement setLocation = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(localPage.setWeatherLocation)));
		String locationActual = setLocation.getText();
		Assert.assertTrue(locationActual.contains("Newcastle upon Tyne"));

	}

}
