package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.driverFactory;

public class navigationPage extends driverFactory {

    @FindBy(xpath = "//*[@id=\"orb-modules\"]/header/div[2]/div[1]/div[1]/nav/ul/li[2]/a")
    public WebElement uk;

    @FindBy(xpath = "//*[@id=\"orb-modules\"]/header/div[2]/div[1]/div[1]/nav/ul/li[3]/a")
    public WebElement world;

    @FindBy(xpath = "//*[@id=\"orb-modules\"]/header/div[2]/div[1]/div[1]/nav/ul/li[4]/a")
    public WebElement business;

    @FindBy(xpath = "//*[@id=\"orb-modules\"]/header/div[2]/div[1]/div[1]/nav/ul/li[5]/a")
    public WebElement politics;


}

