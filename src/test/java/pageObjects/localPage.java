package pageObjects;


import utils.driverFactory;

public class localPage extends driverFactory
{
 
	public String weather = "//div[@class='orb-nav-section orb-nav-links orb-nav-focus']//a[contains(text(),'Weather')]"; // xPath locator
    public String locatorInput = "ls-c-search__input-label"; // ID locator
    public String locationsList = "location-list"; // ID locator
    public String locationsListItem = "ls-c-locations-list-item"; // class Name locator
    public String searchLocation = "//input[@title='Search for a location']"; // xPath locator
    public String setWeatherLocation = "wr-location-name-id"; // ID locator

    

}